#include <Arduino.h>
#include <SoftwareSerial.h>
#include <DFMiniMp3.h>
#include "Mp3Notify.h"

#include <MFRC522.h> //library responsible for communicating with the module RFID-RC522
#include <SPI.h> //library responsible for communicating of SPI bus
#define SS_PIN    21
#define RST_PIN   22
#define SIZE_BUFFER     18
#define MAX_SIZE_BLOCK  16
#define greenPin     12
#define redPin       32

// instance a DFMiniMp3 object,
// defined with the above notification class and the hardware serial class
//
// DFMiniMp3<HardwareSerial, Mp3Notify> mp3(Serial1);

// Some arduino boards only have one hardware serial port, so a software serial port is needed instead.
// comment out the above definition and uncomment these lines
SoftwareSerial secondarySerial(16, 17); // RX, TX
DFMiniMp3<SoftwareSerial, Mp3Notify> mp3(secondarySerial);
MFRC522 mfrc522(SS_PIN, RST_PIN); 

void setup()
{
  Serial.begin(9600);

  Serial.println("initializing...");

  mp3.begin();
  mp3.stop();
  SPI.begin(); // Init SPI bus

  uint16_t volume = mp3.getVolume();
  Serial.print("volume ");
  Serial.println(volume);
  mp3.setVolume(8);
  
  uint16_t count = mp3.getTotalTrackCount();
  
  Serial.println("total tracks:" );
  Serial.println(count);
  Serial.println("starting...");

  // Init MFRC522
  mfrc522.PCD_Init(); 
  Serial.println("Approach your reader card...");
  Serial.println();

}

void waitMilliseconds(uint16_t msWait)
{
  uint32_t start = millis();
  
  while ((millis() - start) < msWait)
  {
    // calling mp3.loop() periodically allows for notifications
    // to be handled without interrupts
    mp3.loop();
    delay(1);
  }
}

boolean played = false;

int convert(int OldValue){
  int OldMin = 0;
  int OldMax = 4096;
  int NewMax = 30;
  int NewMin = 0;
  return (((OldValue - OldMin) * (NewMax - NewMin)) / (OldMax - OldMin)) + NewMin;
}

void loop()
{

  int sensorwert = analogRead(13);
  mp3.setVolume(convert(sensorwert));
  
  delay(100);

  if ( ! mfrc522.PICC_IsNewCardPresent()) 
  {
    return;
  }
  
  // Select a card
  if ( ! mfrc522.PICC_ReadCardSerial()) 
  {
    return;
  }

  //mfrc522.PICC_DumpToSerial(&(mfrc522.uid));

  if(!played){
    Serial.println("track 1 start");
    mp3.playMp3FolderTrack(1);  // sd:/0001/0001.mp3
    Serial.println("track 1 started");
    delay(300);
    Serial.println(mp3.getCurrentTrack());
    played = true;
  }
  

}